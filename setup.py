import setuptools

setuptools.setup(
    name='points_io',
    version='1.1.1',
    package_dir={'': 'src'},
    packages=setuptools.find_packages(where='src'),
    install_requires=['numpy>=1.17'],
    license='MIT',
    author='Michał Kadlof',
    author_email='m.kadlof@cent.uw.edu.pl',
    description='Package for reading PDB files as numpy arrays, and vice versa',
    keywords='PDB protein data bank molecular structures',
    url='https://bitbucket.org/4dnucleome/points_io',
    classifiers=[
        "Programming Language :: Python :: 3",
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering :: Bio-Informatics'
    ],
)
