README
======

Dependencies and setup

python 3.x

    pip install -r requirements.txt
    pip install git+https://bitbucket.org/4dnucleome/points_io.git

Usage
-----

To open PDB file:

    >>> from points_io import point_reader
    >>> data = point_reader("my_file.pdb")

To save PDB file:
    
    >>> from points_io import save_points_as_pdb
    >>> save_points_as_pdb(data, 'my_file.pdb')
    File my_file.pdb saved...
    
Optional arguments to `save_points_as_pdb`:
* `render_connect`, renders or not CONECT records. Default `True`
* `verbose`, write message to stdout after saving. Default `True`
* `save_psf`, write corresponding PSF file, Default `False`

Contact to main author
----------------------

Michał Kadlof <m.kadlof@cent.uw.edu.pl>

License
-------

Copyright 2019 Michał Kadlof

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.