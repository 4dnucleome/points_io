import filecmp
import os
import unittest

import numpy as np

from points_io.hybrid36 import hy36encode
from points_io.points_io import save_points_as_pdb


class TestPointsIo(unittest.TestCase):

    def test_saving_points(self):
        test_file_1 = "test_file.pdb"
        test_file_2 = "test_file_2.pdb"
        ref_file_1 = os.path.join('reference_files', test_file_1)
        ref_file_2 = os.path.join('reference_files', test_file_2)

        dumb_structure = np.ones(30).reshape((10, 3))
        save_points_as_pdb(dumb_structure, test_file_1)
        self.assertTrue(filecmp.cmp(test_file_1, ref_file_1))

        dumb_structure2 = np.ones(30000).reshape((10000, 3))
        save_points_as_pdb(dumb_structure2, test_file_2)
        self.assertTrue(filecmp.cmp(test_file_2, ref_file_2))

        os.remove(test_file_1)
        os.remove(test_file_2)

    def test_encoding(self):
        self.assertEqual(hy36encode(4, 1), '   1')
        self.assertEqual(hy36encode(4, 9999), '9999')
        self.assertEqual(hy36encode(4, 10000), 'A000')


if __name__ == '__main__':
    unittest.main()
