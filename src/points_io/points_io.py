import csv
import os
import re
from typing import List, Union, Tuple

import numpy as np

from points_io.hybrid36 import hy36encode

__version__ = "1.1.1"


def point_reader(fname: str) -> np.ndarray:
    """
    Read the points from PDB file format
    :param fname: path to file or file descriptor
    :return: Nx3 numpy array with positions of points
    """
    reg = re.compile(r'^(ATOM|HETATM)')
    with open(fname) as f:
        atoms = [i.strip() for i in f if re.search(reg, i)]
    points = []
    for i in atoms:
        x = float(i[30:38])
        y = float(i[38:46])
        z = float(i[46:54])
        points.append((x, y, z))
    return np.array(points)


def xyz_point_reader(fname: str, sep='\t') -> np.ndarray:
    """
    Read points from three columns tab separated file
    :param fname: input file name
    :param sep: column delimiter
    :return: Nx3 numpy array with positions of points
    """
    with open(fname) as f:
        reader = csv.reader(f, delimiter=sep)
        points = []
        for line in reader:
            x, y, z = [float(i) for i in line]
            points.append([x, y, z])
        return np.array(points)


def save_points_as_xyz(points: np.array, filename: str, fmt='chimera', **kwargs) -> None:
    """
    Saves points as xyz file
    :param points: Nx3 numpy array with positions of points
    :param filename: str with path
    :param fmt:
                xyz         3 column tab separated
                idxyz       4 column tab separated (first column is an index)
                chimera     kwargs: molecule_name
    :param kwargs: vide fmt
    """
    prefix = ''
    atoms = ''
    suffix = ''
    n = len(points)
    if fmt == 'xyz':
        for i in range(n):
            x, y, z = points[i]
            atoms += ('{}\t{}\t{}\n'.format(x, y, z))
    elif fmt == 'idxyz':
        for i in range(n):
            x, y, z = points[i]
            atoms += ('{}\t{}\t{}\t{}\n'.format(i + 1, x, y, z))
    elif fmt == 'chimera':
        if kwargs is not None and 'molecule_name' in kwargs:
            molecule_name = kwargs['molecule_name']
        else:
            molecule_name = ''
        prefix = '{}\n{}\n'.format(n, molecule_name)
        for i in range(n):
            x, y, z = points[i]
            atoms += ('C\t{}\t{}\t{}\n'.format(x, y, z))

    with open(filename, 'w') as f:
        f.write(prefix)
        f.write(atoms)
        f.write(suffix)
    print("File {} saved...".format(filename))


def save_points_as_pdb(points: np.array, pdb_file_name: str, remarks: List[str] = None,
                       render_connect=True, verbose=True, save_psf=False,
                       custom_connects=None, values=None) -> None:
    """
    Save points in PDB file format.
    :param points: Nx3 numpy array with positions of points
    :param pdb_file_name: str with path
    :param remarks: List of extra comments included in PDB file
    :param render_connect: switch for rendering CONECT records in PDB file
    :param verbose: print or not output on stdout
    :param save_psf: render or not corresponding PSF file (useful for trajectories in DCD file format).
    :param custom_connects: Allow custom connects pattern. Must be list of tuples of 0-based bead indices
    :param values: Allow to add custom values for beads in position of beta-factor
    """
    if remarks:
        remarks.insert(0, '')
        for i in range(len(remarks)):
            remarks[i] = 'REMARK 250 ' + remarks[i]
            if remarks[i][-1] != '\n':
                remarks[i] += '\n'
        remarks = ''.join(remarks)
    else:
        remarks = ''
    atoms = ''
    n = len(points)

    # Wartości do wstawienia w pozycji b-factor
    if not values:
        values = [.0 for _ in range(n)]

    for i in range(n):
        x = points[i][0]
        y = points[i][1]
        try:
            z = points[i][2]
        except IndexError:
            z = 0.0

        atoms += (
            '{0:6}{1:>5}  {2:3}{3:}{4:3} {5:}{6:}{7:}   {8:>8.3f}{9:>8.3f}'
            '{10:>8.3f}{11:6.2f}{12:6.2f}{13:>12}\n'.format("ATOM", hy36encode(5, i + 1),
                                                            'B', ' ', 'BEA', 'A',
                                                            hy36encode(4, i + 1),
                                                            ' ',
                                                            max(x, -999),
                                                            max(y, -999),
                                                            max(z, -999),
                                                            0, values[i], 'B'))
    connects = ''

    # Jeśli użytkownik nie podał własnych conect'ów to łączymy atomy liniowo
    if render_connect and not custom_connects:
        if n != 1:
            connects = 'CONECT    1    2\n'
            for i in range(2, n):
                connects += 'CONECT{}{}{}\n'.format(hy36encode(5, i), hy36encode(5, i - 1), hy36encode(5, i + 1))
            connects += 'CONECT{}{}\n'.format(hy36encode(5, n), hy36encode(5, n - 1))

    # Jeśli użytkownik podał pary punktów to łączymy tak jak użytkownik chciał
    # przeparsujmy listę par do listy sąsiedztwa
    if render_connect and custom_connects:
        # Wyciągamy listę punktów mających sąsiada
        communicating_beads = set()
        for a, b in custom_connects:
            communicating_beads.add(a)
            communicating_beads.add(b)
        communicating_beads = sorted(list(communicating_beads))

        # Inicjalizujemy słownik sąsiedztwa
        neighborhood = {}
        for i in communicating_beads:
            neighborhood[i] = []

        # Wypełniamy słownik sąsiedztwa
        for p1, p2 in custom_connects:
            neighborhood[p1].append(p2)
            neighborhood[p2].append(p1)

        # Sortujemy listy sąsiedztwa
        for i in communicating_beads:
            neighborhood[i].sort()

        # I wreszcie możemy renderować rekordy CONECT
        connects = ''
        for i in communicating_beads:
            # Rekordy connect mogą zawierać do 4-ciu sąsiadów
            # Przy większej liczbie należy powtórzyć rekord z tym samym
            # numerem serial
            while neighborhood[i]:
                r = f'CONECT{hy36encode(5, i + 1)}'
                neig = neighborhood[i][:4]
                neighborhood[i] = neighborhood[i][4:]
                for n in neig:
                    r += f'{hy36encode(5, n + 1)}'
                r += '\n'
                connects += r

    # Łączymy wszystkie sekcje pliku
    pdb_file_content = remarks + atoms + connects

    # Zapisujemy plik
    with open(pdb_file_name, 'w') as f:
        f.write(pdb_file_content)
    if verbose:
        print("File {} saved...".format(pdb_file_name))
    if save_psf:
        base_name, _ = os.path.splitext(pdb_file_name)
        psf_file_name = base_name + '.psf'
        generate_psf(len(points), psf_file_name)
        if verbose:
            print("File {} saved...".format(psf_file_name))


def generate_psf(n: int, file_name: str, title="No title provided"):
    """
    Saves PSF file. Useful for trajectories in DCD file format.
    :param n: number of points
    :param file_name: PSF file name
    :param title: Human readable string. Required in PSF file.
    :return: List with string records of PSF file.
    """
    assert len(title) < 40, "provided title in psf file is too long."
    # noinspection PyListCreation
    lines = ['PSF CMAP\n']
    lines.append('\n')
    lines.append('      1 !NTITLE\n')
    lines.append('REMARKS {}\n'.format(title))
    lines.append('\n')
    lines.append('{:>8} !NATOM\n'.format(n))
    for k in range(1, n + 1):
        lines.append('{:>8} BEAD {:<5}BEA  B    B      0.000000        1.00 0           0\n'.format(k, k))
    lines.append('\n')
    lines.append('{:>8} !NBOND: bonds\n'.format(n - 1))
    for i in range(1, n):
        lines.append('{:>8}{:>8}\n'.format(i, i + 1))
    with open(file_name, 'w') as f:
        f.writelines(lines)


def save_points_as_gro(points: np.array, filename: str, comment="comment") -> None:
    """
    Saves points in GROMACS file format.
    :param points: Nx3 numpy array with positions of points
    :param filename: str with path
    :param comment: Human readable record in gro file.
    """
    n = len(points)
    x, y, z = zip(*points)
    d = max(x + y + z)
    lines = ["{}\n".format(comment), str(n) + '\n']
    for i in range(n):
        x = points[i][0] / 10
        y = points[i][1] / 10
        z = points[i][2] / 10
        w = '{:5}{:5}{:5}{:5}{:8.3f}{:8.3f}{:8.3f}\n'.format(i, "BEA", "B", i + 1, x, y, z)
        lines.append(w)
    lines.append('{0:5f} {0:5f} {0:5f}'.format(d))
    filename = '{}'.format(filename)
    open(filename, 'w').writelines(lines)
    print("File {} saved...".format(filename))


def save_points_as_marker_set(points: Union[List[Tuple[float, float, float]], np.ndarray],
                              cmm_file_name: str,
                              marker_set_name: str = "",
                              marker_colors: Union[None, np.ndarray] = None,
                              marker_radii: Union[None, List[float], np.ndarray, float] = None,
                              marker_notes: Union[None, List[str]] = None,
                              links: Union[None, List[Tuple[int, int]], np.ndarray] = None,
                              links_radii: Union[None, List[float], np.ndarray, float] = None,
                              links_colors: Union[None, np.ndarray] = None,
                              verbose: bool = True):
    """
    Saves file as chimera marker set.
    :param points: Nx3 numpy array with positions of markers
    :param cmm_file_name: str: chimera marker set file name
    :param marker_set_name: str: name of a set
    :param marker_colors: None, (3,) numpy array, or (n, 3) numpy array
    :param marker_radii: None, float, or (n, ) numpy array
    :param marker_notes: None or List of strings
    :param links: List of nodes indices pairs
    :param links_radii: one, float, or (n, ) numpy array
    :param links_colors: None, (3,) numpy array, or (n, 3) numpy array
    :param verbose: bool: print messages or not

    Marker set doc:
        https://www.cgl.ucsf.edu/chimerax/docs/user/markers.html#markerfiles

    """

    def _validate_input():
        if type(marker_colors) == np.ndarray and marker_colors.ndim in [1, 2]:
            assert len(marker_colors) == len(points), "Invalid number of marker colors or array dimension"
        if type(marker_radii) == np.ndarray:
            assert len(marker_colors) == len(points), "Invalid number of marker radii"
        if type(marker_notes) == list:
            assert len(marker_notes) == len(points), "Invalid number of marker notes"
        if type(links_radii) in [list, np.ndarray]:
            assert len(links_radii) == len(links), "Invalid number of link_radii"
        if type(links_colors) == np.ndarray and links_colors.ndim in [1, 2]:
            assert len(links_colors) == len(links), "Invalid number of links colors or array dimension"

    def _normalize_colors(colors):
        "Jeśli kolor jest wektroem liczb, to przekształćmy do maacierz 3,n RGB"
        rgb = []
        if type(colors) == np.ndarray and colors.ndim == 1 and len(colors) > 1:
            color_min = np.min(colors)
            color_max = np.max(colors)
            colors = (colors - color_min) / (color_max - color_min)
            for value in colors:
                r = 0.3 * (1 - value)  # Interpolacja liniowa dla kanału czerwonego
                g = 0.3 + 0.7 * value  # Interpolacja liniowa dla kanału zielonego
                b = 0.3 * (1 - value)  # Interpolacja liniowa dla kanału niebieskiego
                rgb.append((r, g, b))
        rgb = np.array(rgb)
        return rgb

    def _set_marker_color(i: int):
        """set color for i-th marker"""
        r, g, b = 1.0, 1.0, 1.0
        if marker_colors is None:
            return ""
        elif marker_colors.shape == (3,):
            r, g, b = marker_colors
        elif marker_colors.ndim == 2:
            r, g, b = marker_colors[i]
        return f'r="{round(r, 3)}" g="{round(g, 3)}" b="{round(b, 3)}" '

    def _set_marker_radius(i: int):
        """set radius for i-th marker"""
        if marker_radii is None:
            return ""
        if type(marker_radii) in [float, int]:
            return f'radius="{round(marker_radii, 3)}" '
        if type(marker_radii) in [np.ndarray, list]:
            return f'radius="{round(marker_radii[i], 3)}" '

    def _set_marker_notes(i: int):
        if marker_notes is None:
            return ""
        else:
            return f'note="{marker_notes[i]}" '

    def _set_link_radius(i: int):
        """set radius for i-th marker"""
        if links_radii is None:
            return ""
        if type(links_radii) in [float, int]:
            return f'radius="{round(links_radii, 3)}" '
        if type(links_radii) in [np.ndarray, list]:
            return f'radius="{round(links_radii[i], 3)}" '

    def _set_link_color(i: int):
        """set color for i-th marker"""
        r, g, b = 1.0, 1.0, 1.0
        if links_colors is None:
            return ""
        if links_colors.shape == (3,):
            r, g, b = links_colors
        elif links_colors.ndim == 2:
            r, g, b = links_colors[i]
        return f'r="{round(r, 3)}" g="{round(g, 3)}" b="{round(b, 3)}" '

    _validate_input()
    marker_colors = _normalize_colors(marker_colors)
    links_colors = _normalize_colors(links_colors)
    w = f'<marker_set name="{marker_set_name}">\n'
    # Rysujemy markery
    for k, p in enumerate(points):
        x, y, z = points[k]
        x = round(x, 2)
        y = round(y, 2)
        z = round(z, 2)
        marker_color = _set_marker_color(k)
        marker_radius = _set_marker_radius(k)
        marker_note = _set_marker_notes(k)
        w += f'<marker id="{k}" x="{x}" y="{y}" z="{z}" {marker_color}{marker_radius}{marker_note}/>\n'

    # Rysujemy linki pomiędzy markerami
    if links is not None:
        for k, p in enumerate(links):
            p1, p2 = p
            link_radius = _set_link_radius(k)
            link_color = _set_link_color(k)
            w += f'<link id1="{p1}" id2="{p2}" {link_radius}{link_color}/>\n'
    w += '</marker_set>'

    # Zapisujemy plik
    with open(cmm_file_name, 'w') as f:
        f.write(w)
    if verbose:
        print("File {} saved...".format(cmm_file_name))


def main():
    pass


if __name__ == '__main__':
    main()
